import Vue from 'vue'
import ElementUI from 'element-ui'
import lang from 'element-ui/lib/umd/locale/en'
import locale from 'element-ui/lib/locale'
import VueDataTables from 'vue-data-tables'
import Modal1 from "../components/modal_1.vue"
import Modal2 from "../components/modal_2.vue"
import Modal3 from "../components/modal_3.vue"
import axios from 'axios'
import store from './store/store'
import { mapGetters,mapState } from 'vuex'

locale.use(lang)
Vue.use(ElementUI)
Vue.use(VueDataTables)

var employee = new Vue({
  el: '#employee',
  store,
  data(){
    return {
      employees: [],
      total: 0,
      selectedRow: [],
      selectionMessage: '',
      pagination: { pageSizes: [25, 50, 100] },
      tableProps: {
      border: false,
      stripe: true,
      defaultSort: {
        prop: 'flow_no',
        order: 'descending'
        }
      },
      layout: 'pagination, table',
      row_data:'',
      display_show_modal:false,
      row_index:"",
      open_modal3:"false"
    }
  },
  components: {
     Modal1, Modal2, Modal3
 },
 computed: {
    ...mapGetters([
      'employee_data',
      'clicked_employee'
    ]),
   selectionExists() {
     return this.selectedRow.length > 0 && this.selectedRow != null
   }
  },
  updated(){
    this.employees = this.employee_data
     },
  methods: {
    changed: function(event) {
      this.$store.commit('change', event.target.value)
    },
    addDetails(event) {
      this.isActive = true
      this.showForm = true
    },
    addPackage() {
      const employee_ids = []
      if (this.selectedRow.length === 0) {
        alert("Dude focus!!! Select something first")
      } else {
        this.selectedRow.forEach((item) => employee_ids.push(item['id']))
       
      }
    },
    close(event) {
      this.isActive = false
    },
    toggleModal:function(show_modal){
      this.display_show_modal=show_modal
    },
    fetchData(query) {
      axios.get('api/employees', {
        params: {page: query.page, pageSize: query.pageSize}
      })
      .then((response) => {
        this.$store.commit('change_employees', response.data.employees)
        this.employees = this.employee_data
        this.total = response.data.total
        //console.log(this.employee_data)
      });
    },
    handleSelectionChange(val) {
      this.selectionMessage = (val.length == 1) ? "1 Selected" : val.length + " Selected"
      this.selectedRow = val
    },
    toggleSelection() {
      if (this.selectedRow) {
        this.selectedRow.forEach(row => {
          console.log("Mult", this.$refs.multipleTable)
          this.$refs.multipleTable.toggleRowSelection(row);
        });
      } else {
        this.$refs.multipleTable.clearSelection();
      }
    },
    tableListner(row) {
      if (row.name !== null) {
        return [{
          name: row.name,
          handler: _ => {
            this.row_index= this.employee_data.indexOf(row)
            this.$store.commit('change_index', this.row_index)
            console.log(this.clicked_employee)
            this.row_data= this.clicked_employee;
            this.display_show_modal = true
          },
        }]
      }
    },
  }
}) 