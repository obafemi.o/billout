import Vue from 'vue'
import ElementUI from 'element-ui'
import lang from 'element-ui/lib/umd/locale/en'
import locale from 'element-ui/lib/locale'
import VueDataTables from 'vue-data-tables'
import axios from 'axios'
import Modal4 from "../components/modal_4.vue"

// locale.use(lang)
// Vue.use(ElementUI)
// Vue.use(VueDataTables)

var settings = new Vue({
  el: '#settings-package',
  data() {
    return {
      packages: [],
      clickedpackage:'',
      total: 0,
      selectedRow: [],
      pagination: {
        pageSizes: [25, 50, 100]
      },
      tableProps: {
        border: false,
        stripe: true,
        defaultSort: {
          prop: 'flow_no',
          order: 'descending'
        }
      },
      layout: 'pagination, table',
      row_data: '',
      display_show_modal: false,
      row_index: "",
      open_modal3: "false"
    }
  },
  components: {
    Modal4
},
  methods: {
    changed: function (event) {
      this.$store.commit('change', event.target.value)
    },
    addDetails(event) {
      this.isActive = true
      this.showForm = true
    },
    addPackage() {
      const employee_ids = []
      if (this.selectedRow.length === 0) {
        alert("Dude focus!!! Select something first")
      } else {
        this.selectedRow.forEach((item) => employee_ids.push(item['id']))
        console.log(employee_ids)
      }
    },
    close(event) {
      this.isActive = false
    },
    toggleModal: function (show_modal) {
      this.display_show_modal = show_modal
    },
    fetchData(query) {
      console.log("Query:", query)
      axios.get('/api/packages', {
          params: {
            page: query.page
          }
        })
        .then((response) => {
          //this.$store.commit('change_employees', response.data.employees)
          //this.employees = this.employee_data
          
          this.packages =response.data.packages
          this.total = response.data.total
          //console.log(this.employee_data)
        });
    },
    handleSelectionChange(val) {
      this.selectedRow = val
    },
    tableListner(row) {
      if (row.name !== null) {
        return [{
          name: row.name,
          handler: _ => {
            this.clickedpackage = row;
            console.log(this.clickedpackage)
            this.display_show_modal = true
          },
        }]
      }
    },
  }
})
