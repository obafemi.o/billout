import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        employees: [],
        index:'',
      },
      mutations: {
        change_employees(state, employees) {
          state.employees = employees
        },
        change_index(state, index) {
          state.index = index
        },
        update_employee(state, employee){
          Vue.set(state.employees, state.index, employee)
          
        }
        
      },
      getters: {
        employee_data( state){return state.employees},
        clicked_employee(state){return state.employees[state.index]}
      }
})
