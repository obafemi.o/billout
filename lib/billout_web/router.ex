defmodule BilloutWeb.Router do
  use BilloutWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BilloutWeb do
    pipe_through :browser
    post "/employees", EmployeeController, :create
    get "/employees/package/:id", EmployeeController, :retrieve
    get "/", PageController, :index
    get "/employees/sample/download", EmployeeController, :download_sample
    get "/employees/upload", EmployeeController, :new_upload
    post "/employees/upload", EmployeeController, :upload
    put "/employees", EmployeeController, :update
    resources "/employees", EmployeeController
    get "/employees/sort", EmployeeController, :sort
    resources "/packages", PackageController
    get "/settings", SettingsController, :index
    get "/settings/packages", SettingsController, :package
  end

  # Other scopes may use custom stacks.
  scope "/api", BilloutWeb.Api, as: :api do
    pipe_through :api
    resources "/earnings", EarningController
    resources "/employees", EmployeeController, only: [:index, :show]
    resources "/packages", PackageController, only: [:index, :show, :create]
    resources "/countries", CountryController, only: [:index, :show]
    # resources "/employee_package", EmployeePackageController, only: [:show, :create]
    get "/states/:country_id", StateController, :state_country
    get "/earnings/:package_id", EarningController, :package_earnings
    get "/list_packages", PackageController, :list_packages
    # resources "/employee_package", EmployeePackageController
    post "/assign_package", PackageController, :create_employee_package
    # get "/packages/view", PackageController, :view
    post "/earnings/upsert", EarningController, :update_or_insert
  end
end
