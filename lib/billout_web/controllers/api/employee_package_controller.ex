defmodule BilloutWeb.Api.EmployeePackageController do
  use BilloutWeb, :controller

  alias Billout.Organization

  def create(conn, %{"employee_package" => employee_package_params}) do
    IO.inspect "Empl_pac"
    IO.inspect employee_package_params
    case Organization.create_employee_package(employee_package_params) do
      {:ok, employee_package} ->
      json conn, %{employee_package: employee_package}
      {:error, %Ecto.Changeset{} = changeset} ->
        # render(conn, "new.html", changeset: changeset)
      json conn, %{changeset: changeset.errors["Package not Assigned"]}
     end
  end

end
