defmodule BilloutWeb.Api.EarningController do
  use BilloutWeb, :controller

  alias Billout.Organization

  def package_earnings(conn, %{"package_id" => package_id}) do
    earnings = Organization.get_earnings!(package_id)
    render(conn, "package_earnings.json", earnings: earnings)
  end

  def create(conn, %{"employee" => employee_id, "earning" => earning_params}) do
    case Organization.link_employee_package(employee_id, %{tag: ""}) do
      {:ok, package} ->
        package_id= package.id
        case Organization.create_earnings(package_id, earning_params) do
          {:ok, earning} ->
            json conn, %{earning: earning}
          {:error, %Ecto.Changeset{} = changeset} ->
            render(conn, "new.html", changeset: changeset)
          _->
            conn
            json conn,"ok"
        end
    end
  end


  def update_or_insert(conn, %{"package" => package_id, "earning" => earning_params}) do
    case Organization.upsert_earnings(package_id, earning_params) do
      {:ok, earning} ->
        json conn, %{earning: earning}
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
      _->
        conn
        json conn,"ok"
    end
  end

  def show(conn, %{"id" => id}) do
    case Organization.get_employee_package(id) do
      nil ->
        json(conn, [])
      employee_package ->
        IO.inspect "In show"
        IO.inspect employee_package
        earnings = employee_package.package.earnings
        render(conn, "package_earnings.json", earnings: earnings, error: "")
      {:error, %Ecto.Changeset{} = changeset} ->
        json(conn, changeset)
    end
  end
  # def show(conn, %{"id" => id}) do
  #   earning = Organization.get_earnings!(id)
  #   json(conn, %{earning: earning})
  # end
end
