defmodule BilloutWeb.Api.EmployeeController do
  use BilloutWeb, :controller

  alias Billout.Organization

  def index(conn, params) do
    IO.inspect "Paramas"
    IO.inspect params
    cond do
      params == %{} ->
        render(conn, "index.json", employees: [], total: [])
      params ->
        page = String.to_integer(params["page"])
        pageSize = String.to_integer(params["pageSize"])
        offset = if page == 1, do: 0, else: (page - 1) * pageSize
        employees = Organization.list_employees(offset, pageSize)
        total = Organization.get_total()
        render(conn, "index.json", employees: employees, total: total)

      true ->
        render(conn, "index.json", employees: [], total: [])
    end
  end

  @spec show(Plug.Conn.t(), map) :: Plug.Conn.t()
  def show(conn, %{"id" => id}) do
    employee = Organization.get_employee!(id)
    json(conn, employee)
    # render(conn, "show.json", employee: employee)
  end

  def create(conn, %{"employee" => employee_params}) do
    case Organization.create_employee(employee_params) do
      {:ok, employee} ->
        json(conn, %{employee: employee})

      {:error, %Ecto.Changeset{} = changeset} ->
        # render(conn, "new.html", changeset: changeset)
        json(conn, %{changeset: changeset.errors[:title]})
    end
  end
end
