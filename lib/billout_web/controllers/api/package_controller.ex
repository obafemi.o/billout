defmodule BilloutWeb.Api.PackageController do
  use BilloutWeb, :controller


  alias Billout.Organization
  alias Billout.Repo

  def list_packages(conn, _params) do
    packages = Organization.list_packages()
    render(conn, "list_packages.json", packages: packages)
  end
  #  def index(conn, _params) do
  #     packages = Organization.list_packages()
  #     json conn, %{packages: packages}
  #  end

  def create(conn, %{"id"=>id, "package" => package_params}) do
    case Organization.link_employee_package(id, package_params) do
      {:ok, package} ->
        json(conn, %{package: package})

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  
  def index(conn, params) do
    cond do
      params == %{} ->
        render(conn, "index.json", packages: [], total: [])

      params["page"] ->
        page = String.to_integer(params["page"])
        offset = if page == 1, do: 0, else: (page - 1) * 25
        packages = Organization.list_package_templates(offset)
        total = Organization.get_package_total()
        render(conn, "index.json", packages: packages, total: total)

      true ->
        render(conn, "index.json", packages: [], total: [])
    end
  end

  def create_employee_package(conn, %{"employee_id" => employee_id, "package_id" => package_id}) do
    IO.inspect package_id
    IO.inspect employee_id
    case Organization.create_employee_package(employee_id, package_id) do
      {:ok, employee_package} ->
      json conn, %{employee_package: employee_package}
      {:error, %Ecto.Changeset{} = changeset} ->
        # render(conn, "new.html", changeset: changeset)
      json conn, %{changeset: changeset.errors["Package not Assigned"]}
    end
  end

end
