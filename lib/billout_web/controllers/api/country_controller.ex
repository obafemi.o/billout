defmodule BilloutWeb.Api.CountryController do
  use BilloutWeb, :controller

  alias Billout.Location

  def index(conn, _params) do
    countries = Location.list_countries()
    render(conn, "index.json", countries: countries)
  end

  # def show(conn, %{"id" => id}) do

  # end
end
