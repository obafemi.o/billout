defmodule BilloutWeb.SettingsController do
  use BilloutWeb, :controller
  alias Billout.Organization
  alias Billout.Organization.Package

  def index(conn, _params) do
    render(conn, :index)
  end

  def package(conn, _params) do
    render(conn, :package)
  end
end
