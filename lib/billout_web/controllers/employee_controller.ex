defmodule BilloutWeb.EmployeeController do
  use BilloutWeb, :controller

  alias Billout.Organization
  alias Billout.Organization.Employee
  alias NimbleCSV.RFC4180, as: CSV
  alias Billout.Repo
  import Ecto.Changeset

  # def index(conn, params) do
  #   employees = Organization.list_employees(params)
  #   render(conn, "index.html", employees: employees, employees: employees.entries)
  # end

  def index(conn, params) do
    count = Repo.aggregate(Employee, :count, :id)
    search = Organization.search(params)
    page = Employee |> Repo.paginate(params)
    render(conn, "index.html", employees: page.entries, page: page, count: count, search: search)
  end

  def sort(conn, _params) do
    employees = Organization.list_employees_desc()
    render(conn, "index.html", employees: employees)
  end

  @spec new(Plug.Conn.t(), any) :: Plug.Conn.t()
  def new(conn, _params) do
    changeset = Organization.change_employee(%Employee{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"employee" => employee_params}) do
    IO.inspect("employee_params")
    IO.inspect(employee_params)

    case Organization.create_employee(employee_params) do
      {:ok, employee} ->
        json(conn, %{employee: employee})

      {:error, %Ecto.Changeset{} = changeset} ->
        # render(conn, "new.html", changeset: changeset)
        json(conn, %{changeset: changeset.errors[:title]})
    end
  end

  def new_upload(conn, _params) do
    render(conn, "upload.html", conn: conn)
  end

  def download_sample(conn, _params) do
    file = File.read!("priv/samples/sample.csv")

    conn
    |> put_resp_content_type("text/csv")
    |> put_resp_header("content-disposition", "attachment; filename=sample.csv")
    |> send_resp(200, file)
  end

  def upload(conn, %{"employees" => employee_params}) do
    employee_params.path
    |> File.read!()
    |> CSV.parse_string()
    |> validate_file_data(conn)
  end

  defp validate_file_data(params, conn) do
    c =
      Enum.all?(params, fn [
                             title,
                             first_name,
                             surname,
                             middle_name,
                             email,
                             country,
                             state,
                             gender,
                             role,
                             phone,
                             address,
                             address_2,
                             dob,
                             marital_status,
                             children,
                             dependents
                           ] ->
        employee = %{
          "address" => address,
          "address_2" => address_2,
          "children" => children,
          "country" => country,
          "dependents" => String.replace(dependents, "\r", ""),
          "dob" => dob,
          "email" => email,
          "first_name" => first_name,
          "gender" => gender,
          "marital_status" => marital_status,
          "middle_name" => middle_name,
          "phone" => phone,
          "role" => role,
          "state" => state,
          "surname" => surname,
          "title" => title
        }

        Organization.is_file_data_valid?(employee)
      end)

    case c do
      true -> create_file_data(params, conn)
      false -> send_resp(conn, 500, "Error")
    end
  end

  defp create_file_data(params, conn) do
    Enum.each(params, fn [
                           title,
                           first_name,
                           surname,
                           middle_name,
                           email,
                           country,
                           state,
                           gender,
                           role,
                           phone,
                           address,
                           address_2,
                           dob,
                           marital_status,
                           children,
                           dependents
                         ] ->
      employee = %{
        "address" => address,
        "address_2" => address_2,
        "children" => children,
        "country" => country,
        "dependents" => String.replace(dependents, "\r", ""),
        "dob" => dob,
        "email" => email,
        "first_name" => first_name,
        "gender" => gender,
        "marital_status" => marital_status,
        "middle_name" => middle_name,
        "phone" => phone,
        "role" => role,
        "state" => state,
        "surname" => surname,
        "title" => title
      }

      case Organization.create_employee(employee) do
        {:ok, _} ->
          :ok

        {:error, _} ->
          send_resp(conn, 500, "Error")
          :break
      end
    end)

    send_resp(conn, 200, "Success")
  end

  def show(conn, %{"id" => id}) do
    employee = Organization.get_employee!(id)
    # render(conn, "show.html", employee: employee)
    json(conn, %{employee: employee})
  end

  def retrieve(conn, %{"id" => id}) do
    employee_package = Organization.get_employee_package(id)
    # render(conn, "show.html", employee: employee)
    IO.inspect(employee_package)
    json(conn, %{employee_package: employee_package})
  end

  def edit(conn, %{"id" => id}) do
    employee = Organization.get_employee!(id)
    changeset = Organization.change_employee(employee)
    render(conn, "edit.html", employee: employee, changeset: changeset)
  end

  def update(conn, %{"employee" => employee_params}) do
    id = Map.get(employee_params, "id")
    employee = Organization.get_employee!(id)
    IO.inspect("employee")
    IO.inspect(employee_params)

    case Organization.update_employee(employee, employee_params) do
      {:ok, employee} ->
        json(conn, %{employee: employee})

      {:error, %Ecto.Changeset{} = changeset} ->
        json(conn, %{changeset: changeset.errors[:title]})
        # render(conn, "edit.html", employee: employee, changeset: changeset)
    end
  end

  # def update(conn, %{"id" => id, "employee" => employee_params}) do
  #   employee = Organization.get_employee!(id)

  #   case Organization.update_employee(employee, employee_params) do
  #     {:ok, employee} ->
  #       conn
  #       |> put_flash(:info, "Employee updated successfully.")
  #       |> redirect(to: Routes.employee_path(conn, :show, employee))

  #     {:error, %Ecto.Changeset{} = changeset} ->
  #       render(conn, "edit.html", employee: employee, changeset: changeset)
  #   end
  # end

  def delete(conn, %{"id" => id}) do
    employee = Organization.get_employee!(id)
    {:ok, _employee} = Organization.delete_employee(employee)

    conn
    |> put_flash(:info, "Employee deleted successfully.")
    |> redirect(to: Routes.employee_path(conn, :index))
  end

  def assign(conn, %{package_id: pid, employee_id: eid, start_date: sd, end_date: ed}) do
    case Organization.assign_package(pid, eid, sd, ed) do
      {:ok, employee_package} ->
        json(conn, %{employee_package: employee_package})

      {:error, _} ->
        json(conn, %{})
    end
  end
end

# def create(conn, %{"employee" => employee_params}) do
#     case Organization.create_employee(employee_params) do
#       {:ok, employee} ->
#       json conn, %{employee: employee}
#       {:error, %Ecto.Changeset{} = changeset} ->
#         # render(conn, "new.html", changeset: changeset)
#       json conn, %{changeset: changeset.errors[:title]}
#      end
#   end
