defmodule BilloutWeb.PageController do
  use BilloutWeb, :controller

  def index(conn, _params) do
    conn
    |> redirect(to: Routes.employee_path(conn, :index))
  end
end
