defmodule BilloutWeb.Api.PackageView do
  use BilloutWeb, :view

  def render("list_packages.json", %{packages: packages}) do
    %{packages: render_many(packages, BilloutWeb.Api.PackageView, "package.json")}
  end

  def render("index.json", %{packages: packages, total: total}) do
    %{
      total: total,
      packages: render_many(packages, BilloutWeb.Api.PackageView, "package.json")
    }
  end

  def render("package.json", %{package: package}) do
    %{
      id: package.id,
      tag: package.tag,
      description: package.description
    }
  end
end
