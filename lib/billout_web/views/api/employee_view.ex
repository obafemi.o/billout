defmodule BilloutWeb.Api.EmployeeView do
  use BilloutWeb, :view

  def render("index.json", %{employees: employees, total: total}) do
    %{
      total: total,
      employees: render_many(employees, BilloutWeb.Api.EmployeeView, "employee.json")
    }
  end

  def render("show.json", %{employee: employee}) do
    %{data: render_one(employee, BilloutWeb.Api.EmployeeView, "employee.json")}
  end

  def render("employee.json", %{employee: employee}) do
    %{
      id: employee.id,
      title: employee.title,
      name: employee.first_name <> " " <> employee.surname,
      first_name: employee.first_name,
      surname: employee.surname,
      middle_name: employee.middle_name,
      email: employee.email,
      phone: employee.phone,
      gender: employee.gender,
      address: employee.address,
      address2: employee.address_2,
      marital_status: employee.marital_status,
      date_of_birth: employee.dob,
      dob: employee.dob,
      country: employee.country,
      state: employee.state,
      children: employee.children,
      dependents: employee.dependents,
      role: employee.role,
      status: employee.status,
      # packages: render_many(employee.packages, BilloutWeb.Api.EmployeeView, "package.json", as: :packages)
    }
  end

  def render("package.json", %{packages: packages}) do
    # IO.inspect "packages"
    # IO.inspect packages
  end
end
