defmodule BilloutWeb.Api.EarningView do
  use BilloutWeb, :view

  def render("package_earnings.json", %{earnings: earnings, error: error}) do
    IO.inspect "In view Earnings"
    IO.inspect earnings
    # %{data: render_many(earnings, BilloutWeb.Api.EarningView, "earning.json")}
    render_many(earnings, BilloutWeb.Api.EarningView, "earning.json")
  end

  def render("earning.json", %{earning: earning}) do
    %{
      type: earning.type,
      start: earning.startDate,
      end: earning.endDate,
      frequency: earning.frequency,
      rate: earning.rate,
      basis: earning.rate
    }
  end
end
