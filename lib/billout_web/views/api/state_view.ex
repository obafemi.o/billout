defmodule BilloutWeb.Api.StateView do
  use BilloutWeb, :view

  def render("index.json", %{states: states}) do
    %{data: render_many(states, BilloutWeb.Api.StateView, "state.json")}
  end

  def render("state_country.json", %{states: states}) do
    %{data: render_many(states, BilloutWeb.Api.StateView, "state.json")}
  end

  def render("state.json", %{state: state}) do
    %{
      id: state.id,
      name: state.name,
      country_id: state.country_id
    }
  end
end
