defmodule BilloutWeb.Api.CountryView do
  use BilloutWeb, :view

  def render("index.json", %{countries: countries}) do
    %{data: render_many(countries, BilloutWeb.Api.CountryView, "country.json")}
  end

  def render("country.json", %{country: country}) do
    %{
      id: country.id,
      iso2: country.iso2,
      iso3: country.iso3,
      name: country.name,
      phonecode: country.phonecode
    }
  end
end
