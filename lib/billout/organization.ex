defmodule Billout.Organization do
  import Ecto.Query, warn: false
  alias Billout.Repo
  alias Billout.Organization.{Employee, Package, EmployeePackage, Earning}

  def list_employees() do
    Repo.all(Employee)
  end

  def list_employees(offset, limit) do
    Employee
    |> order_by(asc: :first_name, asc: :surname)
    |> limit(^limit)
    |> offset(^offset)
    |> Repo.all()
    |> Repo.preload(:packages)
  end

  def get_total() do
    Repo.aggregate(Employee, :count, :id)
  end

  def search(params) do
    search_term = get_in(params, ["query"])

    Employee
    |> Employee.search(search_term)
    |> Repo.all()
  end

  def list_employees_desc() do
    Employee |> order_by(desc: :first_name) |> Repo.all()
  end

  def get_employee!(id), do: Repo.get!(Employee, id) |> Repo.preload(:packages)

  def create_employee(attrs \\ %{}) do
    IO.inspect("employee")
    IO.inspect(attrs)

    %Employee{}
    |> Employee.changeset(attrs)
    |> Repo.insert()
  end

  def is_file_data_valid?(attrs \\ %{}) do
    changeset =
      %Employee{}
      |> Employee.changeset(attrs)

    changeset.valid?
  end

  def update_employee(%Employee{} = employee, attrs) do
    employee
    |> Employee.changeset(attrs)
    |> Repo.update()
  end

  def delete_employee(%Employee{} = employee) do
    Repo.delete(employee)
  end

  @spec change_employee(Billout.Organization.Employee.t()) :: Ecto.Changeset.t()
  def change_employee(%Employee{} = employee) do
    Employee.changeset(employee, %{})
  end

  # def create_package(attrs \\ %{}) do
  #   %Package{}
  #  |> Ecto.Changeset.change()
  #  |> Ecto.Changeset.put_embed(:earning, attrs )
  #  |> Repo.insert()
  # end

  # def save_package(attrs \\ %{}) do
  #   earnings = Map.get(attrs, :earnings)
  #   %Package{}
  #   |> Ecto.Changeset.change
  #   |> Ecto.Changeset.put_embed(:earnings, earnings)
  #   |> Repo.insert()

  # end

  def list_packages do
    Package
    |> where([p],is_nil(p.tag )== false )
    |> Repo.all()
  end

  def list_package_templates(offset) do
    Package
    |> where([p], is_nil(p.tag) == false)
    |> order_by(asc: :tag)
    |> limit(25)
    |> offset(^offset)
    |> Repo.all()
    |> Repo.preload(:earnings)
  end

  def get_package_total() do
    query = from p in "packages", where: is_nil(p.tag) == false
    Repo.aggregate(query, :count, :tag)
  end

  # def list_packages do
  #   Repo.all(Package)
  # end
  def get_package!(id), do: Repo.get!(Package, id) |> Repo.preload(:earnings)

  # def list_packages do
  #   Package
  #   |> where([p],is_nil(p.tag )== false )
  #   |> Repo.all
  # end
  # def get_package!(id), do: Repo.get!(Package, id) #|> Repo.preload(:employees)

  def create_package(attrs \\ %{}) do
    %Package{}
    |> Package.changeset(attrs)
    |> Repo.insert()
  end

  def link_employee_package(employee_id, package_attrs \\ %{}) do
    employee = Repo.get!(Employee, employee_id)
    {:ok, package} = create_package(package_attrs)

    package
    |> Repo.preload(:employees)
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.put_assoc(:employees, [employee])
    |> Repo.update()
  end

  def create_employee_package(employee_id, package_id) do
    employee = Repo.get!(Employee, employee_id)
    package = Repo.get!(Package, package_id)
    # {:ok, package} = create_package(package_attrs)

    package
    |> Repo.preload(:employees)
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.put_assoc(:employees, [employee])
    |> Repo.update()
  end


  # def update_package(%Package{} = package, attrs) do
  #   package
  #   |> Package.changeset(attrs)
  #   |> Repo.update()
  # end

  # def create_employee_package(attrs \\ %{}) do
  #   IO.inspect attrs
  #   %EmployeePackage{}
  #   |> EmployeePackage.changeset(attrs)
  #   |> Repo.insert()
  # end

  # def delete_package(%Package{} = package) do
  #   Repo.delete(package)
  # end

  # def change_package(%Package{} = package) do
  #   Package.changeset(package, %{})
  # end

  # def link_employee_package(employee_id, package_attrs \\ %{}) do
  #   employee = Repo.get!(Employee, employee_id)
  #   {:ok, package} = create_package(package_attrs)

  def assign_package(pid, eid, sd, ed) do
    EmployeePackage.changeset(%EmployeePackage{}, %{
      package_id: pid,
      employee_id: eid,
      start_date: sd,
      end_date: ed
    })
    |> Repo.insert()
  end

  def get_employee_package(employee_id) do
    # EmployeePackage
    # |> where([ep], ep.employee_id == ^employee_id)
    # |> preload(:package)
    # |> last(:inserted_at)
    # |> Repo.one()
    EmployeePackage
    |> where([ep], ep.employee_id == ^employee_id)
    |> join(:left, [ep], _ in assoc(ep, :package))
    |> join(:left, [_, p], _ in assoc(p, :earnings))
    |> preload([_, p, e], [package: {p, earnings: e}])
    |> Repo.one()
  end

  def create_earning(attrs \\ %{}) do
    %Earning{}
    |> Earning.changeset(attrs)
    |> Repo.insert()
  end

  def create_earnings(package_id, attrs \\ %{}) do
    Enum.map(attrs, fn attr -> handle_create(attr, package_id) end)
  end

  def handle_create(attr, package_id) do
    package = Repo.get!(Package, package_id)

    Ecto.build_assoc(package, :earnings)
    |> Earning.changeset(attr)
    |> Repo.insert()
  end

  def upsert_earnings(package_id, attrs \\ %{}) do
    Enum.map(attrs, fn attr-> handle_upsert(attr, package_id) end)
  end

  def handle_upsert(attr, package_id ) do
    id = Map.get(attr, "id" )
    if is_nil(id)== false do
      Repo.get!(Earning, id )
      |> Earning.changeset(attr)
      |> Repo.update
    end

    if is_nil(id)== true do
      package = Repo.get!(Package, package_id )
      Ecto.build_assoc(package, :earnings)
      |> Earning.changeset(attr)
      |> Repo.insert
    end

  end

   def get_earnings!(package_id) do
    q = from Earning, where: [package_id: ^package_id]
    Repo.all(q)
  end
end
