defmodule Billout.Organization.Earning do
  use Ecto.Schema
  import Ecto.Changeset
  @derive {Jason.Encoder, only: [:basis, :rate, :type, :frequency, :startDate, :endDate]}
  alias Billout.Organization.Package

  schema "earning" do
    field :basis, :string
    field :frequency, :string
    field :rate, :string
    field :type, :string
    field :startDate, :date
    field :endDate, :date
    belongs_to :package, Package

    timestamps()
  end

  @doc false
  def changeset(earning, attrs) do
    earning
    |> cast(attrs, [:type, :rate, :frequency, :basis, :package_id])
    |> validate_required([:type, :rate, :frequency, :basis])
  end
end
