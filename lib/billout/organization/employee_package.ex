defmodule Billout.Organization.EmployeePackage do
  use Ecto.Schema

  @derive {Jason.Encoder, only: [:id, :inserted_at, :updated_at]}
  import Ecto.Changeset
  alias Billout.Organization.{Employee, Package}

  schema "employee_package" do

    belongs_to  :employee, Employee
    belongs_to  :package, Package


    timestamps()
  end

  @doc false
  def changeset(employee_package, attrs) do
    employee_package
    |> cast(attrs, [])
    |> validate_required([])
  end
end
