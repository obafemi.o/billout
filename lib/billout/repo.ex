defmodule Billout.Repo do
  use Ecto.Repo,
    otp_app: :billout,
    adapter: Ecto.Adapters.Postgres

  use Scrivener, page_size: 25
end
