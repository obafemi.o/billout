defmodule Billout.Location do
  import Ecto.Query, warn: false
  alias Billout.Repo
  alias Billout.Location.{Country, State}

  def list_countries do
    Repo.all(Country)
  end

  def get_country!(id), do: Repo.get!(Country, id)

  def create_country(attrs \\ %{}) do
    %Country{}
    |> Country.changeset(attrs)
    |> Repo.insert()
  end

  def update_country(%Country{} = country, attrs) do
    country
    |> Country.changeset(attrs)
    |> Repo.update()
  end

  def delete_country(%Country{} = country) do
    Repo.delete(country)
  end

  def change_country(%Country{} = country) do
    Country.changeset(country, %{})
  end

  def list_states do
    Repo.all(State)
  end

  def get_state!(id), do: Repo.get!(State, id)

  def get_state_country!(country_id) do
    q = from State, where: [country_id: ^country_id]
    Repo.all(q)
  end

  @doc """
  Creates a state.

  ## Examples

      iex> create_state(%{field: value})
      {:ok, %State{}}

      iex> create_state(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_state(attrs \\ %{}) do
    %State{}
    |> State.changeset(attrs)
    |> Repo.insert()
  end

  def update_state(%State{} = state, attrs) do
    state
    |> State.changeset(attrs)
    |> Repo.update()
  end

  def delete_state(%State{} = state) do
    Repo.delete(state)
  end

  def change_state(%State{} = state) do
    State.changeset(state, %{})
  end
end
