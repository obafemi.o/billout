defmodule Billout.LocationTest do
  use Billout.DataCase

  alias Billout.Location

  describe "countries" do
    alias Billout.Location.Country

    @valid_attrs %{
      capital: "some capital",
      currency: "some currency",
      iso2: "some iso2",
      iso3: "some iso3",
      name: "some name",
      phonecode: "some phonecode"
    }
    @update_attrs %{
      capital: "some updated capital",
      currency: "some updated currency",
      iso2: "some updated iso2",
      iso3: "some updated iso3",
      name: "some updated name",
      phonecode: "some updated phonecode"
    }
    @invalid_attrs %{capital: nil, currency: nil, iso2: nil, iso3: nil, name: nil, phonecode: nil}

    def country_fixture(attrs \\ %{}) do
      {:ok, country} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Location.create_country()

      country
    end

    test "list_countries/0 returns all countries" do
      country = country_fixture()
      assert Location.list_countries() == [country]
    end

    test "get_country!/1 returns the country with given id" do
      country = country_fixture()
      assert Location.get_country!(country.id) == country
    end

    test "create_country/1 with valid data creates a country" do
      assert {:ok, %Country{} = country} = Location.create_country(@valid_attrs)
      assert country.capital == "some capital"
      assert country.currency == "some currency"
      assert country.iso2 == "some iso2"
      assert country.iso3 == "some iso3"
      assert country.name == "some name"
      assert country.phonecode == "some phonecode"
    end

    test "create_country/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Location.create_country(@invalid_attrs)
    end

    test "update_country/2 with valid data updates the country" do
      country = country_fixture()
      assert {:ok, %Country{} = country} = Location.update_country(country, @update_attrs)
      assert country.capital == "some updated capital"
      assert country.currency == "some updated currency"
      assert country.iso2 == "some updated iso2"
      assert country.iso3 == "some updated iso3"
      assert country.name == "some updated name"
      assert country.phonecode == "some updated phonecode"
    end

    test "update_country/2 with invalid data returns error changeset" do
      country = country_fixture()
      assert {:error, %Ecto.Changeset{}} = Location.update_country(country, @invalid_attrs)
      assert country == Location.get_country!(country.id)
    end

    test "delete_country/1 deletes the country" do
      country = country_fixture()
      assert {:ok, %Country{}} = Location.delete_country(country)
      assert_raise Ecto.NoResultsError, fn -> Location.get_country!(country.id) end
    end

    test "change_country/1 returns a country changeset" do
      country = country_fixture()
      assert %Ecto.Changeset{} = Location.change_country(country)
    end
  end

  describe "states" do
    alias Billout.Location.State

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def state_fixture(attrs \\ %{}) do
      {:ok, state} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Location.create_state()

      state
    end

    test "list_states/0 returns all states" do
      state = state_fixture()
      assert Location.list_states() == [state]
    end

    test "get_state!/1 returns the state with given id" do
      state = state_fixture()
      assert Location.get_state!(state.id) == state
    end

    test "create_state/1 with valid data creates a state" do
      assert {:ok, %State{} = state} = Location.create_state(@valid_attrs)
      assert state.name == "some name"
    end

    test "create_state/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Location.create_state(@invalid_attrs)
    end

    test "update_state/2 with valid data updates the state" do
      state = state_fixture()
      assert {:ok, %State{} = state} = Location.update_state(state, @update_attrs)
      assert state.name == "some updated name"
    end

    test "update_state/2 with invalid data returns error changeset" do
      state = state_fixture()
      assert {:error, %Ecto.Changeset{}} = Location.update_state(state, @invalid_attrs)
      assert state == Location.get_state!(state.id)
    end

    test "delete_state/1 deletes the state" do
      state = state_fixture()
      assert {:ok, %State{}} = Location.delete_state(state)
      assert_raise Ecto.NoResultsError, fn -> Location.get_state!(state.id) end
    end

    test "change_state/1 returns a state changeset" do
      state = state_fixture()
      assert %Ecto.Changeset{} = Location.change_state(state)
    end
  end
end
