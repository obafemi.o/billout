defmodule Billout.OrganizationTest do
  use Billout.DataCase

  alias Billout.Organization
  alias Billout.Organization.Employee

  test "create_employee/1 returns a customer for valid data" do
    valid_attrs = %{
      "address" => "3 olaletan",
      "children" => "3",
      "country" => "Nigeria",
      "dependents" => "4",
      "dob" => "1960-08-20",
      "email" => "john@example.com",
      "first_name" => "John",
      "marital_status" => "Single",
      "middle_name" => "Joe",
      "phone" => "08033769555",
      "state" => "lagos",
      "surname" => "James",
      "title" => "Mr"
    }

    assert {:ok, employee} = Organization.create_employee(valid_attrs)
  end

  test "create_employee/1 returns a changeset for invalid data" do
    invalid_attrs = %{}
    assert {:error, %Ecto.Changeset{}} = Organization.create_employee()
    invalid_attrs
  end

  describe "packages" do
    alias Billout.Organization.Package

    @valid_attrs %{
      calaculation: "some calaculation",
      frequency: "some frequency",
      rate_amount: "some rate_amount"
    }
    @update_attrs %{
      calaculation: "some updated calaculation",
      frequency: "some updated frequency",
      rate_amount: "some updated rate_amount"
    }
    @invalid_attrs %{calaculation: nil, frequency: nil, rate_amount: nil}

    def package_fixture(attrs \\ %{}) do
      {:ok, package} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Organization.create_package()

      package
    end

    test "list_packages/0 returns all packages" do
      package = package_fixture()
      assert Organization.list_packages() == [package]
    end

    test "get_package!/1 returns the package with given id" do
      package = package_fixture()
      assert Organization.get_package!(package.id) == package
    end

    test "create_package/1 with valid data creates a package" do
      assert {:ok, %Package{} = package} = Organization.create_package(@valid_attrs)
      assert package.calaculation == "some calaculation"
      assert package.frequency == "some frequency"
      assert package.rate_amount == "some rate_amount"
    end

    test "create_package/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Organization.create_package(@invalid_attrs)
    end

    test "update_package/2 with valid data updates the package" do
      package = package_fixture()
      assert {:ok, %Package{} = package} = Organization.update_package(package, @update_attrs)
      assert package.calaculation == "some updated calaculation"
      assert package.frequency == "some updated frequency"
      assert package.rate_amount == "some updated rate_amount"
    end

    test "update_package/2 with invalid data returns error changeset" do
      package = package_fixture()
      assert {:error, %Ecto.Changeset{}} = Organization.update_package(package, @invalid_attrs)
      assert package == Organization.get_package!(package.id)
    end

    test "delete_package/1 deletes the package" do
      package = package_fixture()
      assert {:ok, %Package{}} = Organization.delete_package(package)
      assert_raise Ecto.NoResultsError, fn -> Organization.get_package!(package.id) end
    end

    test "change_package/1 returns a package changeset" do
      package = package_fixture()
      assert %Ecto.Changeset{} = Organization.change_package(package)
    end
  end

  describe "countries" do
    alias Billout.Organization.Country

    @valid_attrs %{
      city_name: "some city_name",
      continent_code: "some continent_code",
      continent_name: "some continent_name",
      country_iso_code: "some country_iso_code",
      country_name: "some country_name",
      geoname_id: "some geoname_id",
      is_in_european_union: "some is_in_european_union",
      locale_code: "some locale_code",
      metro_code: "some metro_code",
      subdivision_1_iso_code: "some subdivision_1_iso_code",
      subdivision_1_name: "some subdivision_1_name",
      subdivision_2_iso_code: "some subdivision_2_iso_code",
      subdivision_2_name: "some subdivision_2_name",
      time_zone: "some time_zone"
    }
    @update_attrs %{
      city_name: "some updated city_name",
      continent_code: "some updated continent_code",
      continent_name: "some updated continent_name",
      country_iso_code: "some updated country_iso_code",
      country_name: "some updated country_name",
      geoname_id: "some updated geoname_id",
      is_in_european_union: "some updated is_in_european_union",
      locale_code: "some updated locale_code",
      metro_code: "some updated metro_code",
      subdivision_1_iso_code: "some updated subdivision_1_iso_code",
      subdivision_1_name: "some updated subdivision_1_name",
      subdivision_2_iso_code: "some updated subdivision_2_iso_code",
      subdivision_2_name: "some updated subdivision_2_name",
      time_zone: "some updated time_zone"
    }
    @invalid_attrs %{
      city_name: nil,
      continent_code: nil,
      continent_name: nil,
      country_iso_code: nil,
      country_name: nil,
      geoname_id: nil,
      is_in_european_union: nil,
      locale_code: nil,
      metro_code: nil,
      subdivision_1_iso_code: nil,
      subdivision_1_name: nil,
      subdivision_2_iso_code: nil,
      subdivision_2_name: nil,
      time_zone: nil
    }

    def country_fixture(attrs \\ %{}) do
      {:ok, country} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Organization.create_country()

      country
    end

    test "list_countries/0 returns all countries" do
      country = country_fixture()
      assert Organization.list_countries() == [country]
    end

    test "get_country!/1 returns the country with given id" do
      country = country_fixture()
      assert Organization.get_country!(country.id) == country
    end

    test "create_country/1 with valid data creates a country" do
      assert {:ok, %Country{} = country} = Organization.create_country(@valid_attrs)
      assert country.city_name == "some city_name"
      assert country.continent_code == "some continent_code"
      assert country.continent_name == "some continent_name"
      assert country.country_iso_code == "some country_iso_code"
      assert country.country_name == "some country_name"
      assert country.geoname_id == "some geoname_id"
      assert country.is_in_european_union == "some is_in_european_union"
      assert country.locale_code == "some locale_code"
      assert country.metro_code == "some metro_code"
      assert country.subdivision_1_iso_code == "some subdivision_1_iso_code"
      assert country.subdivision_1_name == "some subdivision_1_name"
      assert country.subdivision_2_iso_code == "some subdivision_2_iso_code"
      assert country.subdivision_2_name == "some subdivision_2_name"
      assert country.time_zone == "some time_zone"
    end

    test "create_country/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Organization.create_country(@invalid_attrs)
    end

    test "update_country/2 with valid data updates the country" do
      country = country_fixture()
      assert {:ok, %Country{} = country} = Organization.update_country(country, @update_attrs)
      assert country.city_name == "some updated city_name"
      assert country.continent_code == "some updated continent_code"
      assert country.continent_name == "some updated continent_name"
      assert country.country_iso_code == "some updated country_iso_code"
      assert country.country_name == "some updated country_name"
      assert country.geoname_id == "some updated geoname_id"
      assert country.is_in_european_union == "some updated is_in_european_union"
      assert country.locale_code == "some updated locale_code"
      assert country.metro_code == "some updated metro_code"
      assert country.subdivision_1_iso_code == "some updated subdivision_1_iso_code"
      assert country.subdivision_1_name == "some updated subdivision_1_name"
      assert country.subdivision_2_iso_code == "some updated subdivision_2_iso_code"
      assert country.subdivision_2_name == "some updated subdivision_2_name"
      assert country.time_zone == "some updated time_zone"
    end

    test "update_country/2 with invalid data returns error changeset" do
      country = country_fixture()
      assert {:error, %Ecto.Changeset{}} = Organization.update_country(country, @invalid_attrs)
      assert country == Organization.get_country!(country.id)
    end

    test "delete_country/1 deletes the country" do
      country = country_fixture()
      assert {:ok, %Country{}} = Organization.delete_country(country)
      assert_raise Ecto.NoResultsError, fn -> Organization.get_country!(country.id) end
    end

    test "change_country/1 returns a country changeset" do
      country = country_fixture()
      assert %Ecto.Changeset{} = Organization.change_country(country)
    end
  end
end
