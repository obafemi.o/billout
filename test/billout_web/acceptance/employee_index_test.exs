defmodule BilloutWeb.EmployeeIndexTest do
  use Billout.DataCase
  use Hound.Helpers

  hound_session()

  test "presence of employee table" do
    navigate_to("/employees")

    assert page_title() =~ "Billout"
  end

  test "presence of Add Employee button" do
    navigate_to("/employees")
    div = find_element(:class, "modal_1")
    button = find_within_element(div, :tag, "button") |> visible_text()

    assert button == "Add Employee"
  end

  test "presence of Bulk Employee Upload button" do
    navigate_to("/employees")
    div = find_element(:class, "modal_2")
    button = find_within_element(div, :tag, "button") |> visible_text()

    assert button == "Bulk Employee Upload"
  end

  test "presence of Filter button" do
    navigate_to("/employees")
    button = find_element(:class, "filter") |> visible_text()

    assert button == "Filter"
  end

  test "presence of Package tab" do
    navigate_to("/employees")
    tab = find_element(:class, "package") |> visible_text()

    assert tab == "Package"
  end

  test "presence of list of existing packages" do
    navigate_to("/employees")
    tab = find_element(:class, "package") |> visible_text()
    dropdown = find_within_element(tab, :tag, "select") |> visible_text()

    assert button == "Select Package Template"
  end
end
