alias Billout.Organization
alias Billout.Organization.{Package, Earning}
alias Billout.Repo

for data <- 1..100 do
  employee_data = %{
    first_name: Faker.Name.first_name(),
    middle_name: Faker.Name.first_name(),
    surname: Faker.Name.last_name(),
    email: Faker.Internet.email(),
    role: Enum.random(["Engineer", "Lawyer", "Doctor"]),
    gender: Enum.random(["Male", "Female"]),
    phone: Faker.Phone.EnGb.number(),
    address: Faker.Address.street_address(),
    state: Faker.Address.state(),
    country: Faker.Address.country(),
    dob: Faker.Date.date_of_birth(18..65),
    marital_status: Enum.random(["Single", "Married", "Confused"]),
    title: Enum.random(["Mr", "Mrs", "Dr", "Ms", "Miss"]),
    children: Enum.random([0, 1, 2, 3, 4]),
    dependents: Enum.random([0, 1, 2, 3]),
    status:  Enum.random(["Employee", "Contractor", "Director", "Retired", "Resigned", "Discharged"]),
  }

  %Organization.Employee{}
  |> Organization.Employee.changeset(employee_data)
  |> Repo.insert!()
end

for data <- 1..10 do
  package =  %{
    tag:  Enum.random(["Intern", "Developer", "Sales"]),
    description: Faker.Lorem.sentence()
  }

  %Package{}
  |> Package.changeset(package)
  |> Repo.insert!
end

for data <- 1..50 do
  earning = %{
    basis: Enum.random(["Monthly", "Daily", "Hourly"]),
    frequency: Enum.random(["Daily", "Weekly", "Monthly", "Quarterly", "Yearly"]),
    rate: Enum.random(["200000", "150000", "100000"]),
    type: Enum.random(["Salary", "Bonus", "Commission", "Something"]),
    startDate: Faker.Date.between(~D[2019-05-30], ~D[2019-06-30]),
    endDate: Faker.Date.between(~D[2019-06-01], ~D[2019-08-30]),
    package_id: Enum.random([1,2,3,4,5,6,7,8,9,10])
  }

  %Earning{}
  |> Earning.changeset(earning)
  |> Repo.insert!
end
