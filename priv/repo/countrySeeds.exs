alias NimbleCSV.RFC4180, as: CSV
alias Billout.Location.Country
alias Billout.Location.State
alias Billout.Repo

"priv/seed_data/countries.csv"
|> File.read!
|> CSV.parse_string
|> Enum.each(fn [name,iso3,iso2,phonecode,capital,currency]->
  %Country{name: name,iso3: iso3, iso2: iso2, phonecode: phonecode, capital: capital,currency: currency}
  |> Repo.insert
end)

"priv/seed_data/states.csv"
|> File.read!
|> CSV.parse_string
|> Enum.each(fn [name,country_id]->
  %State{name: name,country_id: country_id}
  |> Repo.insert
end)
