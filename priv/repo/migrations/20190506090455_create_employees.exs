defmodule Billout.Repo.Migrations.CreateEmployees do
  use Ecto.Migration

  def change do
    create table(:employees) do
      add :title, :string
      add :first_name, :string
      add :surname, :string
      add :middle_name, :string
      add :email, :string
      add :country, :string
      add :state, :string
      add :gender, :string
      add :role, :string
      add :phone, :string
      add :address, :string
      add :address_2, :string
      add :dob, :date
      add :marital_status, :string
      add :children, :integer
      add :dependants, :integer

      timestamps()
    end
  end
end
