defmodule Billout.Repo.Migrations.AddEmployeeStatus do
  use Ecto.Migration

  def change do
    alter table(:employees) do
      add :status, :string
    end
  end
end
