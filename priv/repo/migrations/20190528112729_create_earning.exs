defmodule Billout.Repo.Migrations.CreateEarning do
  use Ecto.Migration

  def change do
    create table(:earning) do
      add :type, :string
      add :rate, :string
      add :frequency, :string
      add :basis, :string
      add :startDate, :date
      add :endDate, :date
      add :package_id, references(:packages, on_delete: :nothing)

      timestamps()
    end

    create index(:earning, [:package_id])
  end
end
