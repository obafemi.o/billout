# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :billout,
  ecto_repos: [Billout.Repo]

# Configures the endpoint
config :billout, BilloutWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Ulx2cwooyK5s9D5L4rWM6YKmVdbOdervmIzSAQwHXyt2EfqWlYCniKGDqrmwssT3",
  render_errors: [view: BilloutWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Billout.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :scrivener_html,
  routes_helper: Billout.Router.Helpers

# If you use a single view style everywhere, you can configure it here. See View Styles below for more info.
# view_style: :bootstrap

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
